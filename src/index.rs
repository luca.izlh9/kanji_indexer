use std::fs::File;
use std::path::Path;
use std::io::{Read, BufReader, Result as IOResult};
use std::collections::HashMap;
use xml::{EventReader, ParserConfig};
use regex::Regex;

pub struct Index {
    // TODO
}

impl Index {
    pub fn from_xml<P: AsRef<Path>>(path: P) -> IOResult<()> {
        let file = File::open(&path)?;
        let mut file = BufReader::new(file);
        let mut lines = String::new();
        file.read_to_string(&mut lines).unwrap();
        Self::get_doctype_entities(lines);
        
        let file = File::open(&path).unwrap();
        let file = BufReader::new(file);
        let config = ParserConfig::new()
            .add_entity("unc", "unc")
            .add_entity("n", "noun");
        
        let max_events = 10;
        let mut count = 0;
        for e in EventReader::new_with_config(file, config) {
            println!("{:#?}", e);
            count += 1;
            if count >= max_events {
                break
            }
        }
        Ok(())
    }

    fn get_doctype_entities(lines: String) -> HashMap<String, String> {
        let entity_finder = Regex::new(r"^<!ENTITY ([[:alpha:]]+)").unwrap();
        for c in entity_finder.captures("<!ENTITY gikun \"gikun (meaning as reading) or jukujikun (special kanji reading)\">").unwrap().iter() {
            println!("{:?}", c);
        }
        let mut count = 0;
        for line in lines.lines() {
            println!("{:#?}", line);
            count += 1;
            if count > 40 {
                break;
            }
        }
        
        HashMap::new()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn xml_parsing_works() {
        assert!(Index::from_xml("JMDict_e").is_ok())
    }
}
