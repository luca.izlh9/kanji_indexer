use std::convert::TryFrom;

const HIRAGANA_RANGE: (u32, u32) = (0x3041, 0x3096);
const KATAKANA_RANGE: (u32, u32) = (0x30A0, 0x30FF);

#[derive(PartialEq, Eq, Debug)]
enum Reading {
    OnYomi(String),
    KunYomi(String),
}

impl Reading {
    pub fn is_hiragana(reading: &str) -> bool {
        for c in reading.chars().map(|c| c as u32) {
            if c < HIRAGANA_RANGE.0 || c > HIRAGANA_RANGE.1 {
                return false;
            }
        }
        true
    }

    pub fn is_katakana(reading: &str) -> bool {
        for c in reading.chars().map(|c| c as u32) {
            if c < KATAKANA_RANGE.0 || c > KATAKANA_RANGE.1 {
                return false;
            }
        }
        true
    }
}

impl TryFrom<String> for Reading {
    type Error = String;

    fn try_from(value: String) -> Result<Reading, Self::Error> {
        if Reading::is_hiragana(&value) {
            Ok(Reading::KunYomi(value))
        } else if Reading::is_katakana(&value) {
            Ok(Reading::OnYomi(value))
        } else {
            Err(format!("The given String '{}' is not a valid Kanji Reading", value))
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn try_from_string() {
        let on_yomi1 = Reading::try_from("ツウ".to_string()).unwrap();
        let on_yomi2 = Reading::try_from("リョウ".to_string()).unwrap();
        let kun_yomi1 = Reading::try_from("ひと".to_string()).unwrap();
        let kun_yomi2 = Reading::try_from("にち".to_string()).unwrap();

        assert_eq!(on_yomi1, Reading::OnYomi("ツウ".to_string()));
        assert_eq!(on_yomi2, Reading::OnYomi("リョウ".to_string()));
        assert_eq!(kun_yomi1, Reading::KunYomi("ひと".to_string()));
        assert_eq!(kun_yomi2, Reading::KunYomi("にち".to_string()));

        assert!(Reading::try_from("hallo".to_string()).is_err());
        assert!(Reading::try_from("wow".to_string()).is_err());
        assert!(Reading::try_from("人".to_string()).is_err());
    }
}
