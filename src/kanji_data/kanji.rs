const KANJI_UNICODE_RANGES: [(u32, u32); 3] =
    [(0x3400, 0x4db5), (0x4e00, 0x9fcb), (0xf900, 0xfa6a)];

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Debug)]
struct Kanji {
    inner: char,
}

impl Kanji {
    pub fn new(kanji: char) -> Self {
        assert!(
            Kanji::is_kanji(kanji),
            format!("The given character '{}' is not a Kanji", kanji)
        );
        Kanji { inner: kanji }
    }

    pub fn is_kanji(kanji: char) -> bool {
        let as_num = kanji as u32;
        for (from, to) in KANJI_UNICODE_RANGES.iter() {
            if as_num >= *from && as_num <= *to {
                return true;
            }
        }
        false
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::char;

    #[test]
    fn create_valid_kanji() {
        let person = Kanji::new('人');
        let first_common_kanji = Kanji::new('一');
        let last_common_kanji = Kanji::new('龯');
        let first_rare_kanji = Kanji::new('㐀');
        let last_rare_kanji = Kanji::new('䶵');

        let from_num1 = Kanji::new(char::from_u32(KANJI_UNICODE_RANGES[0].0).unwrap());
        let from_num2 = Kanji::new(char::from_u32(KANJI_UNICODE_RANGES[0].1).unwrap());
        let from_num3 = Kanji::new(char::from_u32(KANJI_UNICODE_RANGES[1].0).unwrap());
        let from_num4 = Kanji::new(char::from_u32(KANJI_UNICODE_RANGES[1].1).unwrap());
        let from_num5 = Kanji::new(char::from_u32(KANJI_UNICODE_RANGES[2].0).unwrap());
        let from_num6 = Kanji::new(char::from_u32(KANJI_UNICODE_RANGES[2].1).unwrap());

        println!(
            "{:?} {:?} {:?} {:?} {:?} {:?} {:?} {:?} {:?} {:?} {:?}",
            person,
            first_common_kanji,
            last_common_kanji,
            first_rare_kanji,
            last_rare_kanji,
            from_num1,
            from_num2,
            from_num3,
            from_num4,
            from_num5,
            from_num6
        );
    }
}
